from employee_name import Name
from employee_department import Department
from employee_position import Position
from input_validate import employee_info_input
from user_choice import input_raw_choice
from base import Base


def main():
    choice = input_raw_choice()
    match choice:
        case '0':
            quit()
        case '1':
            employee_info = employee_info_input()
            if employee_info is not None:
                department, name, position = employee_info
                name_examp = Name(name)
                department_examp = Department(department)
                position_examp = Position(position)
                emp_base = Base(department_examp, name_examp, position_examp)
                emp_base.add()
                print(
                    f'Добавлена информация о сотруднике '
                    f'{name_examp.get_name()}'
                    )
        case '2':
            result = Base.get_base()
            if len(result) != 0:
                for info in result:
                    department = info[0].get_department()
                    name = info[1].get_name()
                    position = info[2].get_position()
                    print(
                        f'Сотрудник: {name}',
                        f'    Отдел: {department}',
                        f'Должность: {position}',
                        sep='\n', end='\n\n'
                    )
            else:
                print('Информация о сотрудниках отсутствует')
        case _:
            print('Такого варианта нет.')


if __name__ == '__main__':
    while True:
        main()
