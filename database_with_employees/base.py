class Base:
    """
    Хранение информации о сотрудниках.

    Позволяет добавить информацию о сотруднике в "базу" и получить информацию
    из неё.

    Attributes:
        department:
            Экземпляр класса Department
        name:
            Экземпляр класса Name
        position:
            Экземпляр класса Position

    Methods:
        add():
            Добавляет информацию о сотруднике в "базу"
        get_base():
            Возвращает информацию из "базы".
    """

    database = []

    def __init__(self, department, name, position):
        self.department = department
        self.name = name
        self.position = position

    def __str__(self):
        return f'{self.__class__.__name__}({self.name})'

    def add(self):
        """
        Добавляет информацию о сотруднике в "базу".

        Добавляет в список(атрибут класса) кортеж с информацией о сотруднике.
        """
        Base.database.append((self.department, self.name, self.position))

    @staticmethod
    def get_base():
        """
        Возвращает информацию о работниках из "базы".

        Возвращает список(атрибут класса), в котором хранятся данные
        сотрудников.

        Returns:
            список, каждый элемент которого - информация о сотруднике
        """
        return Base.database
