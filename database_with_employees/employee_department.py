class Department:
    """
    Класс для указания отдела работника.

    Attributes:
        department: str
            отдел, в котором работает сотрудник.

    Methods:
        get_department():
            Возвращает название отдела.
    """

    def __init__(self, department):
        self.department = department

    def __str__(self):
        return f'{self.__class__.__name__}({self.department})'

    def get_department(self):
        """
        Возвращает название отдела.

        Returns:
            название отдела.
        """
        return self.department
