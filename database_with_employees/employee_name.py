class Name:
    """
    Класс для указания должности работника.

    Attributes:
        name: str
            ФИО сотрудника

    Methods:
        get_name():
            Возвращает имя работника.
    """

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f'{self.__class__.__name__}-{self.name}'

    def get_name(self):
        """
        Возвращает имя работника.

        Returns:
            имя работника.
        """
        return self.name
