class Position:
    """
    Класс для указания должности работника.

    Attributes:
        position: str
            должность сотрудника.

    Methods:
        get_position():
            Возвращает название должности.
    """

    def __init__(self, position):
        self.position = position

    def __str__(self):
        return f'{self.__class__.__name__}({self.position})'

    def get_position(self):
        """
        Возвращает название должности.

        Returns:
            название должности.
        """
        return self.position
