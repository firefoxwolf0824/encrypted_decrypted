"""
Ввод данных о пользователе.
"""


def employee_info_input():
    """
    Ввод данных о пользователе.

    Принимает ввод с клавиатуры и возвращает значения, разделив их по запятой.

    Returns:
        Кортеж, содержащий информацию о работнике.
    """
    try:
        department, name, position = map(
            str, input(
                'Через запятую введите отдел, ФИО, должность: '
                ).split(',')
            )
    except ValueError:
        print('Минимум 3 аргумента!')
    else:
        if any(symb in name for symb in '0123456789'):
            print('Имя не может содержать цифры!')
        else:
            return department, name, position
