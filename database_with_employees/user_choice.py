"""
Выбор действия пользователем.
"""


def input_raw_choice():
    """
    Выбор действия пользователем.

    Выводит варианты выбора, принимает ввод с клавиатуры и возвращает
    введенное пользователем значение.
    Returns:
        Введенное с клавиатуры значение.
    """
    print()
    print(
        'Меню выбора действий:',
        '1. Добавить информацию о работнике',
        '2. Вывести информацию о работниках',
        sep='\n')
    choice = input('Выберите нужную Вам цифру: ')
    return choice
